# RP_project

## Introduction
This project implements a robot controller system that computes an optimal path using the A* algorithm between a start and goal point in an input image.

## Prerequisites
- ROS (Robot Operating System) must be installed on your system.
- Ensure that ROS is sourced in your environment:
source /opt/ros/noetic/setup.bash
source ~/catkin_ws/your_workspace/devel/setup.bash

## Usage
1. Put the input image in the "image_dir" directory.
2. Run the following commands to execute the code:

```
rosrun my_robot controller grid_map_node
rosrun my_robot controller start_node
rosrun my_robot controller goal_node
rosrun my_robot controller planner_node
```

4. Use the start and goal nodes to set the coordinates of the start and goal points in the input image.
5. The planner node will compute the optimal path using the A* algorithm and display it in green on the image.

## Notes
- Ensure that the input image is placed in the correct directory before running the code.
- The planner node uses the A* algorithm to compute the optimal path between the specified start and goal points.
- The computed path is displayed in green on the input image.